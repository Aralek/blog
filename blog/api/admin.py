from django.contrib import admin

# Register your models here.
from api.models import Article, Category, Tag
from api.models.article import ShareArticles, LikeArticles
from api.models.comment import Comment

admin.site.register(Article)
admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(LikeArticles)
admin.site.register(ShareArticles)
