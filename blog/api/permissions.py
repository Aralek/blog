from rest_framework.permissions import IsAuthenticatedOrReadOnly

SAFE_METHODS = ('GET', 'HEAD', 'OPTIONS')


class IsAuthor(IsAuthenticatedOrReadOnly):
    def has_object_permission(self, request, view, obj):
        if obj.author == request.user or request.method in SAFE_METHODS:
            return True
        return False
