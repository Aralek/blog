from rest_framework import serializers

from api.models.comment import Comment
from api.serializers.user import UserSerializer


class CommentSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    creation_date = serializers.DateTimeField(format="%d/%m/%Y %H:%M:%S")
    last_modification_date = serializers.DateTimeField(format="%d/%m/%Y %H:%M:%S")

    class Meta:
        model = Comment
        fields = ['id', 'author', 'text', 'is_approved', 'article', 'creation_date', 'last_modification_date']
