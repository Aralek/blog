import datetime
from math import ceil

from rest_framework import serializers

from api.models import Article
from api.serializers.category import CategorySerializer
from api.serializers.tag import TagSerializer
from api.serializers.user import UserSerializer


class ArticleSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    publication_date = serializers.DateTimeField(format="%d/%m/%Y %H:%M:%S")
    category = CategorySerializer()
    tags = TagSerializer(many=True)
    read_time = serializers.SerializerMethodField()
    like_count = serializers.SerializerMethodField()
    share_count = serializers.SerializerMethodField()

    @staticmethod
    def get_read_time(obj):
        # let's say a good reader can read up to 300 words per minute
        # (https://www.mes-exams.com/lecture-rapide/lecture-rapide-test.htm)
        words_count = len(obj.text.split())
        minutes = ceil(words_count / 300)
        time = datetime.time(minute=minutes)
        return datetime.time.strftime(time, "%H:%M:%S")

    @staticmethod
    def get_like_count(obj):
        return obj.user_liked.count()

    @staticmethod
    def get_share_count(obj):
        return obj.user_shared.count()

    class Meta:
        model = Article
        fields = ['id', 'author', 'publication_date', 'visible', 'category', 'title', 'summary', 'text', 'image',
                  'tags', 'read_time', 'like_count', 'share_count']


class LightArticleSerializer(ArticleSerializer):
    text = serializers.SerializerMethodField()

    @staticmethod
    def get_text(obj):
        first_words = " ".join(obj.text.split()[:20]) + "..." if len(obj.text.split()) > 20 else obj.text
        return first_words
