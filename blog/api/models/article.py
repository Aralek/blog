from django.contrib.auth.models import User
from django.db import models
from django.utils.timezone import now

from api.models.category import Category
from api.models.tag import Tag


class Article(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False)
    publication_date = models.DateTimeField(null=True, blank=True, default=now())
    visible = models.BooleanField(default=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length=1025)
    summary = models.TextField(null=True, blank=True)
    text = models.TextField()
    image = models.ImageField(upload_to='images', null=True, blank=True)
    tags = models.ManyToManyField(Tag)
    user_liked = models.ManyToManyField(User, through="LikeArticles", related_name="liked_articles",
                                        related_query_name="liked_articles")
    user_shared = models.ManyToManyField(User, through="ShareArticles", related_name="shared_articles",
                                         related_query_name="shared_articles")

    def __str__(self):
        first_words = " ".join(self.title.split()[:20]) + "..." if len(self.title.split()) > 20 else self.title
        return f"({self.pk}) {first_words}  - by {self.author}"


class LikeArticles(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    liked_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        first_words = " ".join(self.article.title.split()[:20]) + "..." if len(self.article.title.split()) > 20 \
            else self.article.title
        return f"({self.pk}) {self.user.username} liked \"{first_words}\" ({self.article.pk})"


class ShareArticles(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    shared_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        first_words = " ".join(self.article.title.split()[:20]) + "..." if len(self.article.title.split()) > 20 \
            else self.article.title
        return f"({self.pk}) {self.user.username} shared \"{first_words}\" ({self.article.pk})"
