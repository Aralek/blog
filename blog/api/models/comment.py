from django.contrib.auth.models import User
from django.db import models

from api.models import Article


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=False)
    text = models.TextField()
    is_approved = models.BooleanField(default=False)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_modification_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        first_words = " ".join(self.text.split()[:7]) + "..." if len(self.text.split()) > 20 else self.text
        return f"({self.pk}) {first_words} on article {self.article.pk}"
