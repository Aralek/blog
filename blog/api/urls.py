from django.urls import path

from api.views.articles import ArticleView, ArticleDetailView
from api.views.category import CategoryView
from api.views.comments import ArticleCommentsView
from rest_framework_simplejwt import views as jwt_views

from api.views.tag import TagView
from api.views.users import UserView, UserLikedArticlesView, UserSharedArticlesView, UserCommentedArticlesView

urlpatterns = [
    path('token', jwt_views.TokenObtainPairView.as_view(), name='login'),
    path('user', UserView.as_view(), name='user'),

    path('user/liked-articles', UserLikedArticlesView.as_view(), name='liked_articles'),
    path('user/shared-articles', UserSharedArticlesView.as_view(), name='shared_articles'),
    path('user/commented-articles', UserCommentedArticlesView.as_view(), name='commented_articles'),

    path('articles', ArticleView.as_view(), name='articles'),
    path('articles/<int:article_id>', ArticleDetailView.as_view(), name='article_detail'),
    path('articles/<int:article_id>/comments', ArticleCommentsView.as_view(), name='article_comments'),

    path('categories', CategoryView.as_view(), name='categories_list'),
    path('tags', TagView.as_view(), name='tags_list'),
]
