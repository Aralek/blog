from rest_framework.generics import ListAPIView

from api.models import Tag
from api.serializers.tag import TagSerializer


class TagView(ListAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
