from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from api.models import Article
from api.models.comment import Comment
from api.serializers.comment import CommentSerializer


class ArticleCommentsView(ListCreateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = CommentSerializer
    queryset = Article.objects.all()
    lookup_url_kwarg = "article_id"

    def post(self, request, *args, **kwargs):
        article = self.get_object()

        text = request.data.get("text")
        author = self.request.user
        comment = Comment.objects.create(article=article, author=author, text=text, is_approved=True)
        # I'm missing time, so every comments are approved for now

        data = self.get_serializer(comment).data
        return Response(data, status=status.HTTP_201_CREATED)

    def get(self, request, *args, **kwargs):
        article = self.get_object()

        article_comments = Comment.objects.filter(article=article, is_approved=True).order_by("-creation_date")
        data = self.get_serializer(article_comments, many=True).data
        return Response(data, status=status.HTTP_200_OK)
