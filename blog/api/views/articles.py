from django.utils.timezone import now
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from api.models import Article, Category, Tag
from api.permissions import IsAuthor
from api.serializers.article import ArticleSerializer, LightArticleSerializer


class ArticleView(ListCreateAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Article.objects.filter(visible=True).order_by("-publication_date")

    def get_serializer_class(self):
        return LightArticleSerializer if self.request.method == 'GET' else ArticleSerializer

    def post(self, request, *args, **kwargs):
        author = request.user
        category_id = request.data.get('category')
        category = Category.objects.get(id=category_id)
        tag_list = request.data.getlist('tags')
        tags = Tag.objects.filter(id__in=tag_list)
        title = request.data.get('title')
        summary = request.data.get('summary')
        text = request.data.get('text')

        article = Article.objects.create(author=author, category=category, summary=summary, text=text,
                                         publication_date=now(), title=title)

        article.tags.set(tags)
        article.save()

        data = self.get_serializer(article).data

        return Response(data, status=status.HTTP_201_CREATED)


class ArticleDetailView(RetrieveUpdateAPIView):
    serializer_class = ArticleSerializer
    permission_classes = [IsAuthor]
    queryset = Article.objects.all()
    lookup_url_kwarg = "article_id"

    def patch(self, request, *args, **kwargs):
        article = self.get_object()

        category_id = request.data.get('category')
        if category_id:
            category = Category.objects.get(id=category_id)
            article.category = category

        tag_list = request.data.getlist('tags')
        if tag_list:
            tags = Tag.objects.filter(id__in=tag_list)
            article.tags.set(tags)

        title = request.data.get('title')
        if title:
            article.title = title

        summary = request.data.get('summary')
        if summary:
            article.summary = summary

        text = request.data.get('text')
        if text:
            article.text = text

        article.save()

        data = self.get_serializer(article).data

        return Response(data, status=status.HTTP_200_OK)
