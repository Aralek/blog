from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListCreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.models import Article
from api.models.article import LikeArticles, ShareArticles
from api.serializers.article import LightArticleSerializer
from api.serializers.user import UserSerializer


class UserView(CreateAPIView):
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        first_name = request.data.get("first_name")
        last_name = request.data.get("last_name")
        username = request.data.get("username")
        email = request.data.get("email")
        password = request.data.get("password")

        user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username, email=email,
                                        password=password)
        data = self.get_serializer(user).data

        return Response(data, status=status.HTTP_201_CREATED)


class UserLikedArticlesView(ListCreateAPIView):
    serializer_class = LightArticleSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Article.objects.filter(user_liked__in=[user]).order_by("-user_liked__likearticles__liked_at")

    def post(self, request, *args, **kwargs):
        article_id = request.data.get("article")
        article = Article.objects.get(id=article_id)
        liked = request.data.get("liked")
        if liked:
            LikeArticles.objects.create(user=request.user, article=article)
        else:
            LikeArticles.objects.filter(user=request.user, article=article).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserSharedArticlesView(ListCreateAPIView):
    serializer_class = LightArticleSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Article.objects.filter(user_shared__in=[user]).order_by("-user_shared__sharearticles__shared_at")

    def post(self, request, *args, **kwargs):
        article_id = request.data.get("article")
        article = Article.objects.get(id=article_id)
        shared = request.data.get("shared")
        if shared:
            ShareArticles.objects.create(user=request.user, article=article)
        else:
            ShareArticles.objects.filter(user=request.user, article=article).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserCommentedArticlesView(ListAPIView):
    serializer_class = LightArticleSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Article.objects.filter(comment__author=user).order_by("-comment__creation_date")
