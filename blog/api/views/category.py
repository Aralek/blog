from rest_framework.generics import ListAPIView

from api.models import Category
from api.serializers.category import CategorySerializer


class CategoryView(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
