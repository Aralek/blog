import './App.css';

import Article from "./component/article/article";
import ArticleCreation from "./component/articleCreation/articleCreation";
import Home from './component/home/home.js'
import Login from "./component/login/login";
import NavBar from './component/navbar/navbar.js'
import Register from './component/register/register';

import {BrowserRouter as Router, Route, Switch, withRouter} from 'react-router-dom';

import AuthApi from './services/authApi';
import AuthContext from './context/AuthContext';
import {useState} from 'react';


AuthApi.init();

function App() {
    const [isAuth, setIsAuth] = useState(AuthApi.isAuth());
    const NavbarRouter = withRouter(NavBar);

    const contextValue = {
        isAuth,
        setIsAuth,
    }

    return (
        <div>

            <AuthContext.Provider value={contextValue}>

                <Router forceRefresh={true}>
                    <NavbarRouter />

                    <Switch>
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />

                        <Route exact path="/" component={Home}/>
                        <Route exact path="/article" component={ArticleCreation}/>
                        <Route exact path="/article/:id" component={Article}/>

                    </Switch>
                </Router>
            </AuthContext.Provider>

        </div>

    );
}


export default App;
