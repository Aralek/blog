import Card from 'react-bootstrap/Card'
import 'bootstrap/dist/css/bootstrap.min.css';

import './article.css';
import React, { useEffect, useState } from 'react';
import axios from "axios";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const Article = ({ match }) => {

    const [article, setArticle] = useState(null);
    const [comments, setComments] = useState(null);

    useEffect(() => {
        axios.get('http:///127.0.0.1:8000/api/articles/' + match.params.id)
            .then(response => {
                    setArticle(response.data)
                }
            )
            .catch((error) => {
                console.log(error)
            });
    }, [match]);

    useEffect(() => {
        axios.get('http:///127.0.0.1:8000/api/articles/' + match.params.id + '/comments')
            .then(response => {
                    setComments(response.data)
                }
            )
            .catch((error) => {
                console.log(error)
            });
    }, [match]);

    // STATE DES DONNEES USER TYPE TEXT
    const [data, setData] = useState({text: ''})

    // ONCHANGE DES DONNEES TYPE TEXT
    const onChange = (e) => {
        e.persist();
        setData({ ...data, [e.target.name]: e.target.value });
    }

    // ONSUBMIT
    const comment = async (event) => {
        event.preventDefault();
        const dataInput = {text: data.text}
        axios.post('http:///127.0.0.1:8000/api/articles/' + match.params.id + "/comments", dataInput)
            .then((result) => {
                console.log(result.data);
                if (result.data.Status === 'Invalid')
                    alert('Invalid User');
                // Guess I should refresh here, but I don't know how ^^'
            })
    }

    return (

        article ? (
            <div>
                <Card className="w-75 mx-auto my-4 shadow-lg bg-white rounded">
                    <Card.Body className="titleArticlePage">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm mx-auto">
                                    <img
                                        className="d-block imageArticle"
                                        src=""
                                        alt="Article image"
                                    />
                                </div>
                                <div className="col-sm articleInformation">
                                    <p>Title : {article.title} </p>
                                    <p>Summary : {article.summary} </p>
                                    <p>Publication Date : {article.publication_date} </p>
                                    <p>By : {article.author.first_name} {article.author.last_name} (aka {article.author.username})</p>
                                    <p>Reading Time : {article.read_time}</p>
                                    <p>Category : {article.category.name}</p>
                                    <p>Tags : {article.tags.map(tag => (<i>#{tag.name} </i>))}</p>
                                    <p>Text : {article.text} </p>
                                    <p>Likes : {article.like_count}</p>
                                    <p>Shares : {article.share_count}</p>
                                </div>
                            </div>
                        </div>

                    </Card.Body>
                </Card >

                {comments.map(comment => (
                    <Card className="w-75 mx-auto my-4 shadow-lg bg-white rounded">
                        <Card.Body className="titleArticlePage">
                            <div className="container">
                                <div className="row">
                                    <div className="col-sm articleInformation">
                                        <p>Comment of {comment.author.first_name} {comment.author.last_name} (aka {comment.author.username})</p>
                                        <p>Date : {comment.creation_date} (Last modified : {comment.last_modification_date})</p>
                                        <p>{comment.text}</p>

                                    </div>
                                </div>
                            </div>

                        </Card.Body>
                    </Card >
                ))}

                <Card className="w-50 mx-auto my-4 shadow-lg bg-white rounded" inline>
                    <Card.Header as="h3" className="titleCard">Comment</Card.Header>
                    <Card.Body>
                        <Form className="w-50 mx-auto" onSubmit={comment}>
                            <Form.Group controlId="formCommentText">
                                <Form.Label>Text</Form.Label>
                                <Form.Control name="text" onChange={onChange} value={data.text} type="text" placeholder="Enter your text" required />
                            </Form.Group>

                            <Button type="submit" className="buttonRegisterValidation mb-2 w-50">
                                Comment
                            </Button>
                        </Form>

                    </Card.Body>

                </Card>

            </div>
            ) :
            (<h1 className="text-center">... Patience ... La page arrive ...</h1>)
    );
}


export default Article;