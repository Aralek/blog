import './login.css';
import React, { useContext, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import AuthContext from '../../context/AuthContext';
import AuthApi from '../../services/authApi';
import 'bootstrap/dist/css/bootstrap.min.css';


const Login = ({ history }) => {
    const { setIsAuth } = useContext(AuthContext);

    const [credentials, setCredentials] = useState({
        username: '',
        password: ''
    })

    const [error, setError] = useState("")

    const handleChange = (event) => {
        const value = event.currentTarget.value
        const name = event.currentTarget.name

        setCredentials({ ...credentials, [name]: value })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            await AuthApi.auth(credentials);
            setError("");
            setIsAuth(true);
            history.replace('/')
        } catch (errorRequest) {
            setError('error de login')
        }

        console.log(credentials);
    }

    return (
        <div>
            <Card className="w-50 mx-auto my-4 shadow-lg bg-white rounded" inline>
                <Card.Header as="h3" className="titleCard">Log in</Card.Header>
                <Card.Body>
                    <Form className="w-50 mx-auto" onSubmit={handleSubmit}>
                        <Form.Group>
                            <Form.Label>Username</Form.Label>
                            <Form.Control value={credentials.username}
                                onChange={handleChange} type="username" placeholder="Username" name="username"
                                id="username"
                                className={"form-control" + (error && " is-invalid")} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control value={credentials.password}
                                onChange={handleChange} type="password" placeholder="Password" name="password"
                                id="password" />
                        </Form.Group>
                        <div>
                            <Button type="submit" className="buttonLogin mb-2">
                                Log In
                        </Button>
                        </div>
                        <Button href="/register" className="buttonRegister">No account ?<strong>Register !</strong></Button>
                    </Form>

                </Card.Body>

            </Card>
        </div>






    );
}


export default Login;