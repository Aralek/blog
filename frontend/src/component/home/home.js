import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import axios from "axios";
import React, {useEffect, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import './home.css';


const Home = () => {

    let [latestArticles, setLatestArticle] = useState([])

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/articles')
            .then(response => {
                    setLatestArticle(response.data);
                }
            )
            .catch((error) => {
                console.log(error)
            })
    }, []);


    return (
        latestArticles ? (
                <div className="test">
                    <Card className="w-75 mx-auto my-4 shadow bg-white rounded">
                        <Card.Body>

                            <h4 className="testText">ARTICLES LIST</h4>
                            <div className="container justify-content-center">
                                <div className="row">
                                    {latestArticles.map(latest => (
                                        <div className="col-lg-3 pt-3">
                                            <Card className="my-4 shadow bg-white rounded text-center concert__card" inline>
                                                <div className="row no-gutters">
                                                    <div className="col-auto">
                                                        <Card.Img variant="left" src=""
                                                                  style={{width: "15rem", objectFit: "cover"}}/>
                                                    </div>
                                                    <div className="col">
                                                        <div className="card-block px-2">
                                                            <Card.Body>
                                                                <Card.Text>
                                                                    <p><a href={`/article/${latest.id}`}
                                                                          className="text-decoration-none">Title: {latest.title}</a>
                                                                    </p>
                                                                    <p>Text: {latest.text}</p>
                                                                    <p>Author: {latest.author.first_name} {latest.author.last_name} (aka {latest.author.username})</p>
                                                                    <p>Publication Date: {latest.publication_date}</p>
                                                                    <p>Category: {latest.category.name}</p>
                                                                    <p>Tags: {latest.tags.map(tag => (
                                                                        <i>#{tag.name} </i>))}</p>
                                                                    <p>Read Time: {latest.read_time}</p>
                                                                    <p>Like Count: {latest.like_count}</p>
                                                                    <p>Share Count: {latest.share_count}</p>
                                                                </Card.Text>
                                                                <Button className="mb-4 button__card" variant="primary"
                                                                        href={`/article/${latest.id}`}>Read</Button>
                                                            </Card.Body>
                                                        </div>
                                                    </div>


                                                </div>
                                            </Card>
                                        </div>

                                    ))}

                                </div>
                            </div>
                        </Card.Body>
                    </Card>

                </div>
            ) :
            (<h1 className="text-center">... Patience ... La page arrive ...</h1>)

    );
}

export default Home;