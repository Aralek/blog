import './register.css';
import React, { useState } from 'react';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';


const Register = ({ history }) => {

    // STATE DES DONNEES USER TYPE TEXT
    const [data, setData] = useState({
        email: '', username: '', password: '', first_name: '', last_name: ''
    })

    // ONCHANGE DES DONNEES TYPE TEXT
    const onChange = (e) => {
        e.persist();
        setData({ ...data, [e.target.name]: e.target.value });
    }

    // ONSUBMIT
    const registration = async (event) => {
        event.preventDefault();
        const dataInput = {
            email: data.email, username: data.username, password: data.password, first_name: data.first_name, last_name: data.last_name
        }
        axios.post("http://localhost:8000/api/user", dataInput)
            .then((result) => {
                console.log(result.data);
                if (result.data.Status === 'Invalid')
                    alert('Invalid User');
                else
                    history.push('/login')
            })
    }

    return (
        <div>
            <Card className="w-50 mx-auto my-4 shadow-lg bg-white rounded" inline>
                <Card.Header as="h3" className="titleCard">Register</Card.Header>
                <Card.Body>
                    <Form className="w-50 mx-auto" onSubmit={registration}>
                        <Form.Group controlId="formRegisterUsername">
                            <Form.Label>Username*</Form.Label>
                            <Form.Control name="username" onChange={onChange} value={data.username} type="text" placeholder="Username" required />
                        </Form.Group>
                        <Form.Group controlId="formRegisterFirstName">
                            <Form.Label>First Name*</Form.Label>
                            <Form.Control name="first_name" onChange={onChange} value={data.first_name} type="text" placeholder="First Name" required />
                        </Form.Group>
                        <Form.Group controlId="formRegisterName">
                            <Form.Label>Last Name*</Form.Label>
                            <Form.Control name="last_name" onChange={onChange} value={data.last_name} type="text" placeholder="Last Name" required />
                        </Form.Group>
                        <Form.Group controlId="formRegisterEmail">
                            <Form.Label>Email*</Form.Label>
                            <Form.Control name="email" onChange={onChange} value={data.email} type="email" placeholder="Email" required />
                        </Form.Group>
                        <Form.Group controlId="formRegisterPassword">
                            <Form.Label>Password*</Form.Label>
                            <Form.Control name="password" onChange={onChange} value={data.password} type="password" placeholder="Password" required />
                            <Form.Text className="text-muted">
                                Your password must contains at least 8 character, including at least a letter, a capital
                                letter and a number (0-9).
                            </Form.Text>
                        </Form.Group>



                        <Form.Group controlId="formRegisterPasswordConfirmation">
                            <Form.Label>Password Confirmation*</Form.Label>
                            <Form.Control name="passwordConfirmation" type="password" placeholder="Password" required />
                        </Form.Group>

                        <Button type="submit" className="buttonRegisterValidation mb-2 w-50">
                            Register
                        </Button>
                    </Form>

                </Card.Body>

            </Card>
        </div>

    );
}


export default Register;