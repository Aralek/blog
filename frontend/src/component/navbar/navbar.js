import 'bootstrap/dist/css/bootstrap.min.css';
import './navbar.css';
import React, { useState } from 'react';
import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'

import AuthContext from '../../context/AuthContext';
import AuthApi from '../../services/authApi';


const NavBar = ({ history }) => {

  const { isAuth, setIsAuth } = useContext(AuthContext);

  const handleLogout = () => {
    AuthApi.logout();
    setIsAuth(false);
    history.push('/login');
  }

  return (
    <div>


      <div className="header__style">

        <Navbar bg="light" expand="lg" className="fondNavbar">

          <Navbar.Toggle aria-controls="responsive-navbar-nav" />

          <Navbar.Collapse id="responsive-navbar-nav">

            <Nav className="m-auto">

              <Nav.Link href="/" >Home</Nav.Link>
              {isAuth && (
                  <Nav.Link href="/article" >Create an Article</Nav.Link>
              )
              }
              <Nav.Link href="/login" >Login</Nav.Link>
              <Nav.Link href="/register" >Register</Nav.Link>

            </Nav>

            <Nav>
              {/* eslint-disable-next-line no-mixed-operators */}
              {isAuth && (
                  <Nav.Link onClick={handleLogout} href="#" >Logout</Nav.Link>
              )
              }

            </Nav>

          </Navbar.Collapse>

        </Navbar>

      </div>

    </div >

  );
}

export default NavBar;