import Card from 'react-bootstrap/Card'
import 'bootstrap/dist/css/bootstrap.min.css';

import './article.css';
import React, { useEffect, useState } from 'react';
import axios from "axios";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const ArticleCreation = ({ history }) => {

    // STATE DES DONNEES USER TYPE TEXT
    const [data, setData] = useState({
        title: '', summary: '', text: '', category: ''
    })

    let [categories, setCategories] = useState([])

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/categories')
            .then(response => {
                    console.log(response.data);
                setCategories(response.data);
                }
            )
            .catch((error) => { console.log(error) })
    }, []);

    // ONCHANGE DES DONNEES TYPE TEXT
    const onChange = (e) => {
        e.persist();
        setData({ ...data, [e.target.name]: e.target.value });
    }

    // ONSUBMIT
    const article = async (event) => {
        event.preventDefault();
        const dataInput = {
            title: data.title, summary: data.summary, text: data.text, category: data.category
        }
        axios.post("http://localhost:8000/api/articles", dataInput)
            .then((result) => {
                console.log(result.data);
                if (result.data.Status === 'Invalid')
                    alert('Invalid User');
                else
                    history.push('/article/' + result.data.id)
            })
    }

    return (
        <div>
            <Card className="w-50 mx-auto my-4 shadow-lg bg-white rounded" inline>
                <Card.Header as="h3" className="titleCard">Register</Card.Header>
                <Card.Body>
                    <Form className="w-50 mx-auto" onSubmit={article}>
                        <Form.Group controlId="formArticleTitle">
                            <Form.Label>Title*</Form.Label>
                            <Form.Control name="title" onChange={onChange} value={data.title} type="title" placeholder="Title" required />
                        </Form.Group>
                        <Form.Group controlId="formArticleSummary">
                            <Form.Label>Summary</Form.Label>
                            <Form.Control name="summary" onChange={onChange} value={data.summary} type="text" placeholder="Summary" />
                        </Form.Group>
                        <Form.Group controlId="formArticleText">
                            <Form.Label>Text*</Form.Label>
                            <Form.Control name="text" onChange={onChange} value={data.text} type="text" placeholder="Article content" required />
                        </Form.Group>

                        <Form.Group controlId="exampleForm.SelectCustom">
                            <Form.Control as="select" name="salleId" value={data.category} onChange={onChange} className="form-control">
                                {categories.map(category => (
                                    //L'attribut key est obligatoire pour le dataBinding
                                    <option key={category.id} value={category.id}>{category.name}</option>
                                ))}


                            </Form.Control>
                        </Form.Group>

                        <Button type="submit" className="buttonRegisterValidation mb-2 w-50">
                            Create Article
                        </Button>
                    </Form>

                </Card.Body>

            </Card>
        </div>

    );
}


export default ArticleCreation;