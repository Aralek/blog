Voici comment mettre en place la partie backend du projet.

Le backend est en django, vous aurez donc besoin d'installer python 3 : https://www.python.org/downloads/
Une fois python installé, il faut installer les librairies dont nous aurons besoin (django, entre autres) :

py -m pip install django djangorestframework djangorestframework-simplejwt django-cors-headers

Une fois les librairies installées, il faut créer la base de données, et faire les migrations :

Il n'est pas nécéssaire de le faire ici (parce qu'elles sont déjà dans le git) mais sachez que la commande pour faire les migrations est :
py manage.py makemigrations

Par contre, vous devez entrez la commande suivante (la bdd est créée si elle n'existe pas):
py manage.py migrate


Le terminal devrait vous donner des informations après chaque commande. Si ce n'est pas le cas, allez dans le fichier manage.py et supprimez la première ligne:
#!/usr/bin/env python

Vous allez vouloir avoir un superuser (si je peux mettre des données fictives, je vous donnerai les credentials du superuser)
py manage.py createsuperuser

Lancez le serveur:
py manage.py runerver


Il tourne sur http://127.0.0.1:8000
Vous pouvez aller à cette url pour avoir l'admin : http://127.0.0.1:8000/admin il faudra vous connecter avec les credentials du superuser que vous venez de créer.

Si besoin, vous trouverez la liste des urls de l'API dans le fichier urls.py

